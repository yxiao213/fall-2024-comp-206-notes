 ">": Redirects output to a file (overwriting).
 ">>": Appends output to a file.
 "<": Takes input from a file.
 |: Pipes output between commands.
 2>: Redirects error messages to a file.
 &> or 2>&1: Combines output and errors into a file.
